﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TP_08___Kimousse.Classes;
using Region = TP_08___Kimousse.Classes.Region;

namespace TP_08___Kimousse
{
    public partial class FrmBrasserie : Form
    {
        string connectionString;
        List<Brasserie> brasseries;
        List<Region> regions;

        public FrmBrasserie()
        {
            InitializeComponent();
        }

        private void FrmBrasserie_Load(object sender, EventArgs e)
        {
            connectionString = "Server=127.0.0.1;" +
                                "Port=3306;" +
                                "Persist Security Info=False;" +
                                "CharSet=utf8;" +
                                "User Id=kimousse;" +
                                "Password=;" +
                                "Database=kimousse;";

            brasseries = new List<Brasserie>();
            regions = new List<Region>();

            // connexion
            MySqlConnection connection = new MySqlConnection();
            connection.ConnectionString = connectionString;
            try
            {
                connection.Open();
            }
            catch(MySqlException ex)
            {
                MessageBox.Show("Erreur de connexion à la base de données.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }

            // chargement des brasseries
            MySqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `brasserie`;";
            cmd.Prepare();
            MySqlDataReader reader = cmd.ExecuteReader();

            while(reader.Read())
            {
                int _id;
                string _nom;
                string _ville;
                int _id_region;

                _id = reader.GetInt32("id");
                _nom = reader.GetString("nom");
                if (reader.IsDBNull(2))
                {
                    _ville = null;
                }
                else
                {
                    _ville = reader.GetString("ville");
                }
                _id_region = reader.GetInt32("id_region");

                Brasserie item = new Brasserie()
                {
                    Id = _id, Nom = _nom, Ville = _ville, IdRegion = _id_region
                };

                brasseries.Add(item);
            }
            reader.Close();

            lstBrasserie.DisplayMember = "nom";
            lstBrasserie.DataSource = brasseries;

            // chargement des régions
            cmd = connection.CreateCommand();
            cmd.CommandText = @"SELECT * FROM `region`;";
            cmd.Prepare();
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                int _id;
                string _nom;
                int _id_pays;

                _id = reader.GetInt32("id");
                _nom = reader.GetString("nom");
                _id_pays = reader.GetInt32("id_pays");

                Region item = new Region()
                {
                    Id = _id,
                    Nom = _nom,
                    IdPays = _id_pays
                };

                regions.Add(item);
            }
            reader.Close();

            cmbRegion.DisplayMember = "nom";
            cmbRegion.DataSource = regions;

            // todo 01 : mettre les champs nom et ville en lecture seule

            // todo 02 : désactiver la liste déroulante des régions
        }

        private void lstBrasserie_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBrasserie.DataSource != null)
            {
                Brasserie brasserie = (Brasserie)lstBrasserie.SelectedItem;
                txtId.Text = brasserie.Id.ToString();
                txtNom.Text = brasserie.Nom;
                txtVille.Text = brasserie.Ville;
                Region region = regions.Find(r => r.Id == brasserie.IdRegion);
                cmbRegion.SelectedItem = region;
            }
        }

        private void btnNouveau_Click(object sender, EventArgs e)
        {
            // todo 03 : vider tous les champs (id, nom, ville, région)
        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            // todo 04 : insérer dans la table brasserie la nouvelle brasserie
            //           en utilisant les informations saisie dans les champs

            // todo 05 : mettre à jour la liste déroulante des brasseries
        }

        private void btnModifier_Click(object sender, EventArgs e)
        {
            // todo 06 : mettre à jour la table brasserie 
            //           en utilisant les informations saisie dans les champs

            // todo 07 : mettre à jour la liste déroulante des brasseries
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            // todo 08 : demander confirmation de la suppression de la brasserie

            // todo 09 : supprimer dans la table brasserie la brasserie sélectionnée
            //           la brasserie sélectionnée

            // todo 10 : mettre à jour la liste déroulante des brasseries
        }

        // todo 11 : modifier le code source afin de bloquer les boutons 
        //           lorsqu'on n'est pas sensé les utiliser

        // todo 12 : modifier le code source afin de bloquer les champs de saisie et la liste déroulante 
        //           lorsqu'on n'est pas sensé les utiliser

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            // todo 13 : Quitter l'application
            //           Bravo, vous avez terminé votre travail :)
        }
    }
}
